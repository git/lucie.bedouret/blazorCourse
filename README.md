# Blazor Project
***
<br/>

### Description

For this project, we worked in pair with Lucie Bedouret and Lilian Breton. 
This project was done in Blazor for the front end, and c# for the back.

### Start the application

To start our project, you have to be on the 'master' branch. Then, open our project on Visual studio, and you will be able to see our files.
If you want to start the project and see our incredible application, you have to launch the API before the project, or at least in the same time. 
for this, you can go on the project launch options and chose to launch the 2 projects at the same time. 
Otherwise, you can launch the API on one visual studio, and our project on a second one.
The work we did is located on the Inventory page. You can find it in the menu on the left.

### Other precisions

You may get an exception when you will move an item from the inventory for the first time (This error only appears on the first move).
We don't know where does it come from, Lucie never saw it but Lilian has this problem each time he launchs the application. 
If this happens, no problem, just press continue one time, and everything will work perfectly after that. 